package controllers.test

import java.util.concurrent.TimeUnit

import play.api.Logger
import controllers.TodoListController
import models.ConnectTodoList
import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite

import scala.concurrent.Await
import play.api.libs.json.Json
import play.api.test.FakeRequest
import reactivemongo.bson.BSONObjectID

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.Future
import play.api.test.Helpers._
import models.Tasks

class TodoListControllerSpec extends PlaySpec with MockFactory with GuiceOneAppPerSuite {

  val mockConnectTodoList = mock[ConnectTodoList]
  val mockListData = Tasks(None, "title", "date", "description", None)

  /* GET */
  "Should Success from getAllTasks" in {
    val mockListTasks = Future(Seq(mockListData))

    // given
    val controller = new TodoListController(mockConnectTodoList)
    (mockConnectTodoList.getTasks _).expects().returning(mockListTasks)

    // when
    val requestObject = FakeRequest(GET, "/api/v1/tasks")
    val response = controller.getAllTasks().apply(requestObject)
    val actual = Await.result(response, Duration(3000, TimeUnit.MILLISECONDS))

    // then
    assert(actual.header.status == OK)
  }

  /* UPDATE */
  "Should Success from updateTask" in {
    val updateTask = Json.obj(
      "title" -> "title",
      "date" -> "date",
      "description" -> "description"
    )
    val id = BSONObjectID.generate()
    val mockListTasks = Future(Option(mockListData))

    // given
    val controller = new TodoListController(mockConnectTodoList)
    (mockConnectTodoList.updateTasks _).expects(id, mockListData).returning(mockListTasks)

    // when
    val requestObject = FakeRequest(PUT, s"/api/v1/task/$id").withBody(updateTask)
    val response = controller.updateTask(id)(requestObject)
    val actual = Await.result(response, Duration(3000, TimeUnit.MILLISECONDS))

    // then
    assert(actual.header.status == OK)
  }

  /* DELETE */
  "Should Success from deleteTask" in {
    val id = BSONObjectID.generate()
    val mockListTasks = Future(Option(mockListData))

    // given
    val controller = new TodoListController(mockConnectTodoList)
    (mockConnectTodoList.deleteTasks _).expects(id).returning(mockListTasks)

    // when
    val requestObject = FakeRequest(DELETE, s"/api/v1/task/$id")
    val response = controller.deleteTask(id).apply(requestObject)
    val actual = Await.result(response, Duration(3000, TimeUnit.MILLISECONDS))

    // then
    assert(actual.header.status == OK)
  }

  /* POST */
  "Should Failure from addNewTask" in {
    val addNewTask = Json.obj(
      "title" -> Seq("title"),
      "date" -> "date",
      "description" -> "description"
    )

    // given
    val controller = new TodoListController(mockConnectTodoList)
    (mockConnectTodoList.addTasks _)

    // when
    val requestObject = FakeRequest(POST, "/api/v1/task").withBody(addNewTask)
    val response = controller.addNewTask().apply(requestObject)
    val actual = Await.result(response, Duration(3000, TimeUnit.MILLISECONDS))

    // then
    assert(actual.header.status == BAD_REQUEST)
  }
}
