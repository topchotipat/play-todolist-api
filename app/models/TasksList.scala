package models

import javax.inject.Inject
import play.api.Logger
import play.api.libs.json.Json
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.{Cursor, ReadPreference}
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import scala.concurrent.{ExecutionContext, Future}

case class Tasks(_id: Option[BSONObjectID], title: String, date: String, description: String, tasksComplete: Option[Boolean])

object JsonFormats{
  implicit val todoFormat = Json.format[Tasks]
}

class ConnectTodoList @Inject()(implicit ec: ExecutionContext, reactiveMongoApi: ReactiveMongoApi){
  import JsonFormats._

  def todoListCollection: Future[JSONCollection] = reactiveMongoApi.database.map(_.collection("TasksList"))

  def getTasks: Future[Seq[Tasks]] = {
    val query = Json.obj()
    todoListCollection.flatMap(_.find(query)
      .cursor[Tasks](ReadPreference.primary)
      .collect[Seq](100, Cursor.FailOnError[Seq[Tasks]]())
    )
  }

  def addTasks(tasks: Tasks): Future[WriteResult] = {
    todoListCollection.flatMap(_.insert(tasks))
  }

  def deleteTasks(id: BSONObjectID): Future[Option[Tasks]] = {
    val selector = BSONDocument("_id" -> id)
    todoListCollection.flatMap(_.findAndRemove(selector).map(_.result[Tasks]))
  }

  def updateTasks(id: BSONObjectID, tasks: Tasks): Future[Option[Tasks]] = {
    val selector = BSONDocument("_id" -> id)
    val updateModifier = BSONDocument(
      "$set" -> BSONDocument(
        "title" -> tasks.title,
        "date" -> tasks.date,
        "description" -> tasks.description,
        "tasksComplete" -> tasks.tasksComplete
      )
    )
    todoListCollection.flatMap(
      _.findAndUpdate(selector, updateModifier, fetchNewObject = true).map(_.result[Tasks])
    )
  }
}