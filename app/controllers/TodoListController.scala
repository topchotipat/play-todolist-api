package controllers

import javax.inject.Inject
import play.api.Logger
import models.JsonFormats._
import models.{ConnectTodoList, Tasks}
import play.api.libs.json.Json
import play.api.mvc.{Controller}
import reactivemongo.bson.BSONObjectID
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TodoListController @Inject()(cntodo: ConnectTodoList ) extends Controller {

  def getAllTasks() = Action.async {
    cntodo.getTasks.map{ tasks =>
      Ok(Json.toJson(tasks))
    }
  }

  def addNewTask() = Action.async(parse.json){ req =>
    req.body.validate[Tasks].map{ tasks =>
      val changeId = tasks.copy(_id = Some(BSONObjectID.generate()))
      cntodo.addTasks(changeId).map{ _ =>
        Created(Json.toJson(changeId))
      }
    }.getOrElse(Future.successful(BadRequest("Invalid format")))
  }

  def deleteTask(tasksId: BSONObjectID) = Action.async{ req =>
    cntodo.deleteTasks(tasksId).map {
      case Some(tasks) => Ok(Json.toJson(tasks))
      case None => NotFound
    }
  }

  def updateTask(tasksId: BSONObjectID) = Action.async(parse.json){ req =>
    req.body.validate[Tasks].map{ tasks =>
      cntodo.updateTasks(tasksId, tasks).map {
        case Some(tasks) => Ok(Json.toJson(tasks))
        case None => NotFound
      }
    }.getOrElse(Future.successful(BadRequest("Invalid format")))
  }

}
